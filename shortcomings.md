# Possible shortcomings of Data Usage Metrics
This document discusses shortcomings of data usage metrics compliant to the "Code of practice for research data usage metrics 1" (called "standard" in the following).

Critique of usage metrics commonly follows one of the following patterns:
* The score of a dataset is too low/high, because ... (validity problems)
* The score of datasets x and y are not comparable, because ... (comparability problems)
* You cannot use usage metrics in context z, because ... (context problems)

Two categories:
* Handled shortcomings: Problems with usage statistics which have been mitigated by the standards' design.
* Unhandled shortcomings: Problems which are inherent in the concept of usage statistics
  and need to be considered when the metric scores are used for decision making or scientific conclusions.

## Handled shortcomings
* *Duplication problem*: The score of a dataset is too low, because it is published on several repositories. This problem is mitigated by making persistent and globally unique idnentifier mandatory. This allows to aggregate usage statistics of one dataset over different repositories.
* *Good bot problem*: The score of a dataset is too high, because requests from bots (crawlers, spiders, etc.) are counted equally to "human" requests. The standard excludes these access patterns from the reporting; it clearly states criteria under which automated access should be reported nevertheless (e.g., researchers using scripts for data integration).
* *Timliness problem*: The scores for datasets x and y are not comparable, because they were released at two distant points in time. The standard circumvents this problem by making the reporting of the publication year mandatory. The adaptability of the time resolution is another tool for temporal contextualisation of the scores.

## Unhandled shortcomings
* *Versioning problem*: The score of a dataset is too low, because its predecessors or successors are not counted (e.g. Preprint/Postprint).
* *Lost twin problem*: The score of a dataset is too low, because it was (re-)published under a different PID.
* *Granularity problem*: The score of a dataset is too high/too low, because some requests/investigations are only targeted at components of the dataset. The standard mitigates this problem by mirroring the granularity in the reporting. The problem how to properly address metrics for components on their own is postponed by the standard, though.
* *Normalization problem*: The scores for datasets x and y are not comparable, because the number of people typically interested in datasets like x is bigger/smaller than the according number for y. Methods to estimate a normalization procedure are either biased by the data themselves or by preconceptions.
* *Forgery problem (Bad bot problem)*: The score of a dataset is too high, because someone artificially increased the number of investigations and requests, probably with the help of machines and circumventing the measurement to exclude bots. This includes intentional and non-intentional forgeries.
* *Bandwagon problem*: You cannot use usage metrics to definitely determine impact of the scored dataset, because already popular datasets are more likely to get increased usage scores. Comparisons between datasets are more likely to be skewed, the bigger the distance between the scores is.
* *Trust problem*: You cannot use usage metrics when arbitrary repositories report their scores, because there needs to be trust with regard to the numbers reported and with regard to the correct implementation of the standard.
* *Quality-correlation problem*: You cannot use usage metrics alone to determine quality of the dataset, because usage metrics do not necessarily correlate with quality: A "bad example" could easily get hig access numbers. Zero or very low access numbers do not indicate low quality (a "good" dataset could have not been promoted).
* *Aggregation problem*: The score of datasets x_1, ..., x_n are too low, since their usage happens only as a composite. Heterogeneous datasets pose specific problems (what part is really used?)
* *streaming data*: The score of dataset x is too low, since streaming data are used differently than static data.
* *fancy usage problem*: The score of a dataset is too low, since a fancy usage context is not counted/reported.
* *Overinterpretation problem*: You cannot use usage metrics in context z, because it does not fit the context, but it is still used.

# Mitigation strategies for unhandled shortcomings

What is an acceptable risk for a certain problem? How likely will it happen? Will it average out?

## Users of the usage statistics
* Only compare scores of two datasets in the same or in a similar context (e.g. if they are originated in the same community/discipline).
* If only one dataset is scrutinized, contextualize it (e.g. the average score in this community/discipline is x, outliers is this, distribution is that, time correlation, Zero-values).
* Consult other metrics (e.g. data benchmarks).

## Providers of the usage statistics
* Implement a counter-measure strategy for forgeries (IP-blacklisting, incident reporting, impact estimations).
* Execute audits and certification processes.

## Standard committee
* Standardize revokation processes for metrics (partially and time-based).

# References

* [Code of practice for research data usage metrics release 1 (aka "the standard")](https://peerj.com/preprints/26505/)
* [The COUNTER Code of Practice for Release 5](https://www.projectcounter.org/code-of-practice-five-sections/abstract/)
