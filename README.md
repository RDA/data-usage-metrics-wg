# Data Usage Metrics WG

Resources related to the RDA working group "Data Usage Metrics"

See: 
* https://rd-alliance.org/groups/data-usage-metrics-wg
* https://peerj.com/preprints/26505.pdf